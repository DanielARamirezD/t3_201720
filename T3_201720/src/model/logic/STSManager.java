package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import api.ISTSManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;;

public class STSManager implements ISTSManager
{
	public Queue<BusUpdateVO> cola = new Queue<BusUpdateVO>();
	public DoubleLinkedList<StopVO> ls = new DoubleLinkedList<StopVO>();
	
	public void readBusUpdate(File rtFile)	
	{ 
		
		try 
		{	
			InputStream stream = new FileInputStream(rtFile);
			JsonReader reader = new JsonReader(new InputStreamReader(stream));
			Gson gson = new GsonBuilder().create();

			// Read file in stream mode
			reader.beginArray();
			while (reader.hasNext())
			{
				// Read data into object model
				BusUpdateVO buses = gson.fromJson(reader, BusUpdateVO.class);
				cola.enqueue(buses);
				System.out.println(cola);
				
			}
			reader.close();
		}
		catch (UnsupportedEncodingException ex)
		{

		} 
		catch (IOException ex)
		{

		}
	}

	
//	4.	Implemente el m�todo listStops (Integer tripID), 
//	que retorna el nombre de todas las paradas por las que 
//	pas� un viaje especifico indic�ndolas de la �ltima a la primera. 
//	Se dice que un bus pas� por una parada si este estuvo en una 
//	ubicaci�n a 70 metros a la redonda de la parada. 
	
	
	public IStack<StopVO> listStops(Integer tripID) 
	{
		IStack<StopVO> a = null;
		double lat1 = 0;
		double lon1 = 0;
		double lat2 = 0;
		double lon2 = 0;
				
		for (int i = 0; i < cola.getSize(); i++) 
		{
			if(cola.getElement(i).getTripID() == tripID)
			{
				lat1 = cola.getElement(i).getLat();
				lon1 = cola.getElement(i).getLon();
				for (int j = 0; j < ls.getSize(); j++) 
				{
					lat2 = ls.getElement(j).getLat();
					lon2 = ls.getElement(j).getLon();
					double b = getDistance(lat1, lon1, lat2, lon2);
					if(b <= 70)
					{
						a.push(ls.getElement(j));
					}
				}
			}
		}
		
		return a;
	}

	public void loadStops() 
	{
		File file = new File("data/stops.txt");

		try 
		{
			StopVO s = null;
			BufferedReader ln = new BufferedReader(new FileReader(file));
			String linea = ln.readLine();

			while(linea != null)
			{
				String[] datos = linea.split(",");
				String stopID = datos[0];
				String stopCode = datos[1];
				String stopName = datos[2];
				String stopDesc = datos[3];
				String stopLat = datos[4];
				String stopLon = datos[5];
				String zoneID = datos[6];
				String stopURL = datos[7];
				String locationType = datos[8];
				String parentStation = datos[9];
				s = new StopVO(stopID, stopCode, stopName, stopDesc, stopLat, stopLon, zoneID, stopURL, locationType, parentStation);
				ls.add(s);
				linea = ln.readLine();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	public double getDistance(double lat1, double lon1, double lat2, double lon2) 
	{
		final int R = 6371*1000; // Radious of the earth
		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;
		return distance;
	}

	private Double toRad(Double value) 
	{
		return value * Math.PI / 180;
	}

}

