package model.vo;

public class BusUpdateVO 
{
	private String vehicleNo;
	
	private int tripId;
	
	private String direction;
	
	private String destination;
	
	private String pattern;
	
	private double latitude;
	
	private double longitude;
	
	private String recordedTime;
	
	private Object routeMap;
	
	public BusUpdateVO(String nVehicleNo, int nTripId, String nDirection, String nDestination, String nPattern, double nLatitude, double nLongitude, String nRecordedTime, Object nRouteMap)
	{
		vehicleNo = nVehicleNo;
		tripId = nTripId;
		direction = nDirection;
		destination = nDestination;
		pattern = nPattern;
		latitude = nLatitude;
		longitude = nLatitude;
		recordedTime = nRecordedTime;
		routeMap = nRouteMap;
	}
	
	public int getTripID()
	{
		return tripId;
	}
	
	public double getLat()
	{
		return latitude;
	}
	
	public double getLon()
	{
		return longitude;
	}
}
