package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

import model.data_structures.DoubleLinkedList.DoubleNode;

public class DoubleLinkedList<T>  implements IList<T>
{

	private int size;

	private int referenceNode;

	private DoubleNode<T> firstNode;

	private DoubleNode<T> lastNode;

	public DoubleLinkedList()
	{
		firstNode = null;
		lastNode = null;
		size = 0;
		referenceNode = 0;
	}

	public DoubleNode<T> giveFirstNode()
	{
		return firstNode;
	}

	public DoubleNode<T> giveLastNode()
	{
		return lastNode;
	}

	public int giveSize()
	{
		return size;
	}

	public int giveReferenceNode()
	{
		return referenceNode;
	}

	public class DoubleNode<Q>
	{
		private int index;

		private DoubleNode<Q> nextNode;

		private DoubleNode<Q> previousNode;

		private Q data;

		private DoubleNode(Q nData)
		{
			index = 0;
			nextNode = null;
			previousNode = null;
			data = nData;
		}

		public int giveIndex()
		{
			return index;
		}

		public Q giveData()
		{
			return data;
		}

		public DoubleNode<Q> giveNextNode() 
		{
			return nextNode;
		}

		public DoubleNode<Q> givePreviousNode()
		{
			return previousNode;
		}

		public void changeNextNode(DoubleNode<Q> nNextNode)
		{
			if (next()) 
			{
				nextNode = nNextNode;	
			}
		}

		public void changePreviousNode(DoubleNode<Q> nPreviousNode)
		{
			if (previous())
			{
				previousNode = nPreviousNode;
			}	
		}

		public boolean addNode(DoubleNode<Q> nNode)
		{	
			DoubleNode<Q> nodeToAdd = nNode;

			nodeToAdd.changePreviousNode(this);

			nodeToAdd.changeNextNode(this.giveNextNode());

			this.changeNextNode(nNode);

			nodeToAdd.index = size + 1;

			return true;
		}

		public boolean deleteNode(DoubleNode<Q> nNode)
		{
			DoubleNode<Q> nodeToDelete = nNode;

			if (nodeToDelete.givePreviousNode() != null && nodeToDelete.giveNextNode() != null)
			{
				nodeToDelete.givePreviousNode().changeNextNode(nodeToDelete.giveNextNode());
				nodeToDelete.giveNextNode().changePreviousNode(nodeToDelete.givePreviousNode());

				nodeToDelete.index = size - 1 ;

			}
			return true;

		}
	}

	public Iterator<T> iterator() 
	{
		return new Iterator<T>()
		{
			DoubleNode<T> actual = null;

			public boolean hasNext() 
			{
				boolean itHas = false;

				if (size == 0)
				{
					return itHas;
				}
				else if (actual == null)
				{
					return true;
				}
				else if (actual.giveNextNode() != null)
				{
					itHas = true;
				}
				return itHas;
			}

			public T next()
			{
				if (size == 0)
				{
					throw new NoSuchElementException();
				}
				else if (actual == null) 
				{
					actual = firstNode;
					return actual.data;
				}
				else if(actual.giveNextNode() == null)
				{
					throw new NoSuchElementException();
				}
				else
				{
					actual = actual.giveNextNode();
					return actual.data;
				}
			}
		};
	}

	public Integer getSize() 
	{
		return size;
	} 

	public void addAtEnd(T elem)
	{
		DoubleNode<T> nodeToAdd = new DoubleNode<T>(elem);

		if (size == 0)
		{
			firstNode = nodeToAdd;
			nodeToAdd.index = 1;
			referenceNode = firstNode.giveIndex();
			size++;
		}
		else
		{
			DoubleNode<T> temporalNode = firstNode;

			while(temporalNode.giveNextNode() != null)
			{
				temporalNode = temporalNode.giveNextNode();
			}

			if (temporalNode.addNode(nodeToAdd))
			{
				size++;
			}
		}
	}

	public void addAtK(T elem,int k) 
	{
		if (size != 0)
		{			
			if (referenceNode != size)
			{
				if (referenceNode == k)
				{
					add(elem);
				}
			}
		}
	}

	public T deleteFirst()
	{
		if (size == 0)
		{
			throw new NoSuchElementException();
		}
		
		DoubleNode<T> nNode = firstNode;
		
		nNode = nNode.giveNextNode();
		nNode.previousNode = null;
		
		size--;
		return nNode.data;
	}

	public T deleteLast()
	{
		if (size == 0)
		{
			throw new NoSuchElementException();
		}
		
		DoubleNode<T> actual = lastNode;
		
		actual = actual.previousNode;
		
		actual.previousNode.changeNextNode(null);
		
		return actual.data;
	}
	
		

	public void delete(T elem)
	{
		DoubleNode<T> nodeToDelete = new DoubleNode<T>(elem);

		if (size != 0)
		{

			DoubleNode<T> temporalNode = firstNode;

			Iterator<T> iter = iterator();

			boolean deleted = false;

			while(iter.hasNext() && !deleted)
			{
				T temporalElem = iter.next();

				if (temporalElem == firstNode) 
				{
					deleteFirst();
				}
				else if (temporalElem == lastNode)
				{
					temporalNode = lastNode;

					lastNode = temporalNode.givePreviousNode();

					lastNode.changeNextNode(null);

					size--;					
				}
			}

			if (temporalNode.deleteNode(nodeToDelete))
			{
				size--;
			}
		}

	}

	public void deleteAtK(T elem,int k) 
	{
		if (size != 0)
		{			
			if (referenceNode == k)
			{
				delete(elem);
			}	
		}
	}

	public T getElement(int idNode) 
	{
		T element = null;

		if (size != 0)
		{
			DoubleNode<T> temporal1 = firstNode;

			if (temporal1.index == idNode)
			{
				element = firstNode.data;
				referenceNode = idNode;
			}
			else
			{
				boolean found = false;
				Iterator<T> iter = iterator();
				while (iter.hasNext() && !found)
				{
					T data = iter.next();
					temporal1 = new DoubleNode<T>(data);
					if (temporal1.index == idNode)
					{
						element = data;
						referenceNode = idNode;
						found = true;
					}
				}
			}
		}
		return element;
	}

	public boolean next() 
	{
		if (referenceNode + 1 > size)
		{
			return false;
		}
		else
		{
			referenceNode++;
			return true;
		}
	}

	public boolean previous()
	{
		if (referenceNode - 1 < 0)
		{
			return false;
		}
		else
		{
			referenceNode--;
			return true;
		}
	}

	public void add(T elem) 
	{
		DoubleNode<T> nodeToAdd = new DoubleNode<T>(elem);

		if (size == 0)
		{
			firstNode = nodeToAdd;
			nodeToAdd.index++;
			referenceNode = firstNode.index;
			size++;
		}
		else
		{
			DoubleNode<T> temporalNode = firstNode;

			if (temporalNode.addNode(nodeToAdd)) 
			{
				size++;
			}
		}
	}




}

