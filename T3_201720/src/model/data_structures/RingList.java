package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RingList<T> implements IList<T>
{
	private int size;
	
	private DoubleRingNode<T> firstNode;
	
	private int referenceNode;
	
	public RingList()
	{
		firstNode = null;
		
		size = 0;
		
		referenceNode = 0;
	}
	
	public DoubleRingNode<T> giveFirstNode()
	{
		return firstNode;
	}
	
	public class DoubleRingNode<Q>
	{
		private DoubleRingNode<Q> nextNode;
		
		private DoubleRingNode<Q> previousNode;
		
		private int index;
		
		private Q data;
		
		public DoubleRingNode (Q nData)
		{
			data = nData;
			index = 0;
			nextNode = null;
			previousNode = null;
		}
		
		public Q giveData()
		{
			return data;
		}
		
		public int giveIndex()
		{
			return index;
		}
		
		public DoubleRingNode<Q> giveNextNode()
		{
			return nextNode;
		}
		
		public DoubleRingNode<Q> givePreviousNode()
		{
			return previousNode;
		}
		
		public void changeNextNode(DoubleRingNode<Q> nNode)
		{
			nextNode = nNode;
		}
		
		public void changePreviousNode(DoubleRingNode<Q> nNode)
		{
			previousNode = nNode;
		}
		
		public boolean addNode(DoubleRingNode<Q> nNode)
		{
			DoubleRingNode<Q> nodeToAdd = nNode;
			
			nodeToAdd.changePreviousNode(this);
			
			nodeToAdd.changeNextNode(this.giveNextNode());
			
			this.changeNextNode(nodeToAdd);
			
			nodeToAdd.index = size + 1;
			
			return true;
		}
		
		public boolean deleteNode(DoubleRingNode<Q> nNode)
		{
			DoubleRingNode<Q> nodeToDelete = nNode;
			
			if (nodeToDelete.givePreviousNode() != null && nodeToDelete.giveNextNode() != null)
			{
				nodeToDelete.givePreviousNode().changeNextNode(nodeToDelete.giveNextNode());
				nodeToDelete.giveNextNode().changePreviousNode(nodeToDelete.givePreviousNode());
				
				nodeToDelete.index = size - 1 ;
				
				return true;
			}
			return false;
		}
	}

	public Iterator<T> iterator() 
	{
		return new Iterator<T>()
		{
			DoubleRingNode<T> actual = null;

			public boolean hasNext() 
			{
				boolean itHas = false;

				if (size == 0)
				{
					return itHas;
				}
				else if (actual == null)
				{
					itHas = true;
				} 
				else if (actual.giveNextNode() != null)
				{
					itHas = true;
				}
				return itHas;
			}

			public T next()
			{
				if (size == 0)
				{
					throw new NoSuchElementException();
				}
				else if (actual == null) 
				{
					actual = firstNode;
					return actual.data;
				}
				else if(actual.giveNextNode() == null)
				{
					throw new NoSuchElementException();
				}
				else
				{
					actual = actual.giveNextNode();
					return actual.data;
				}
			}
		};
	}

	public Integer getSize() 
	{
		return size;
	}

	public void add(T elem) 
	{
		DoubleRingNode<T> nodeToAdd = new DoubleRingNode<T>(elem);
		
		if (size == 0)
		{
			firstNode = nodeToAdd;
			
			nodeToAdd.index = 1;
			
			referenceNode = firstNode.index;
			
			size++;
		}
		else
		{
			DoubleRingNode<T> temporalNode = firstNode;
			
			if (temporalNode.addNode(nodeToAdd)) 
			{
				size++;
			}
		}
	}

	public void addAtEnd(T elem)
	{
		DoubleRingNode<T> nodeToAdd = new DoubleRingNode<T>(elem);
		
		if (size == 0)
		{
			firstNode = nodeToAdd;

			nodeToAdd.index = 1;
			
			referenceNode = firstNode.index;
			
			size++;
		}
		else
		{
			// en caso de que sea el ultimo?????
			
		}
	}

	public void addAtK(T elem, int k)
	{
		DoubleRingNode<T> nodeToAdd = new DoubleRingNode<T>(elem);
		
		if (size != 0)
		{			
			if (referenceNode != size)
			{
				DoubleRingNode<T> temporalNode = firstNode;
				while(temporalNode.giveIndex() < k)
				{
					if (nodeToAdd.addNode(temporalNode)) 
					{
						nodeToAdd.index = temporalNode.giveIndex();
						size++;
					}
					temporalNode.index++;
				}
			}
		}
	}

	public T getElement(int idNode) 
	{
		T element = null;
		
		if (size != 0)
		{
			DoubleRingNode<T> temporal = firstNode;
			
			if (temporal.index == idNode)
			{
				element = firstNode.data;
				referenceNode = idNode;
			}
			boolean found = false;
			Iterator<T> iter = iterator();
			while (iter.hasNext() && !found)
			{
				T data = iter.next();
				temporal = new DoubleRingNode<T>(data);
				if (temporal.index == idNode)
				{
					element = data;
					referenceNode = idNode;
					found = true;
				}
			}
		}
		return element;
	}

	public void delete(T elem)
	{
		DoubleRingNode<T> nodeToDelete = new DoubleRingNode<T>(elem);
		if (size != 0)
		{
			DoubleRingNode<T> temporalNode = firstNode;
			
			if (temporalNode.deleteNode(nodeToDelete))
			{
				size--;
			}
			
		}
	}

	public void deleteAtK(T elem, int k)
	{
		if (size != 0)
		{			
			if (referenceNode != size)
			{
				if (referenceNode == k)
				{
					add(elem);
				}
			}
		}
	}

	public boolean next()
	{

		referenceNode++;
		return true;
	}

	public boolean previous() 
	{
		referenceNode--;
		return true;
	}

}
