package model.data_structures;

public class Stack<E> implements IStack<E>
{
	private DoubleLinkedList<E> doubleLinkedList;

	public Stack()
	{
		doubleLinkedList = new DoubleLinkedList<E>();
	}
	
	public DoubleLinkedList<E> giveListStack()
	{
		return doubleLinkedList;
	}
	
	public void push(E item) 
	{
		doubleLinkedList.addAtEnd(item);
	}

	public E pop() 
	{
		return doubleLinkedList.deleteLast();
	}
	
	public int giveSizeStack()
	{
		return doubleLinkedList.giveSize();
	}
}
