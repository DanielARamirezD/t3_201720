	package model.data_structures;

public class Queue<E> implements IQueue<E> 
{	
	private DoubleLinkedList<E> doubleLinkedList;
	
	public Queue()
	{
		doubleLinkedList = new DoubleLinkedList<E>();
	}
	
	public DoubleLinkedList<E> giveListQueue()
	{
		return doubleLinkedList;
	}
		
	public void enqueue(E item) 
	{
		doubleLinkedList.addAtEnd(item);
	}

	public E dequeue() 
	{
		return doubleLinkedList.deleteFirst();
	}
	
	public int getSize()
	{
		return doubleLinkedList.getSize();
	}
	
	public E getElement(int idNode)
	{
		return doubleLinkedList.getElement(idNode);
	}
	
}
