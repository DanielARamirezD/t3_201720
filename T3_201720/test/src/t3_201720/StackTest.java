package t3_201720;


import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest extends TestCase
{
	Stack<String> stack = new Stack<String>();
	
	public void setUpEscenario1()
	{
		stack.push("A");
	}
	
	public void setUpEscenario2()
	{
		stack.push("B");
		stack.push("C");
	}
	
	public void testPush()
	{
		// Probando con el stack vacio.
		stack.push("A");
		String a = stack.giveListStack().giveFirstNode().giveData();
		assertEquals("A", a);
		
		//Probando con un elemento en el stack.
		stack.push("B");
		String b = stack.giveListStack().giveFirstNode().giveData();
		assertEquals("B", b);
	}
	
	public void testPop()
	{
		//Probando con un elemento en el stack.
		setUpEscenario1();
		String a = stack.pop();
		assertEquals("A", a);
		
		//Probando con dos elementos en el stack.
		setUpEscenario2();
		String b = stack.pop();
		assertEquals("B", b);
		
		String c = stack.pop();
		assertEquals("C", c);
		
		try 
		{
			stack.pop();
			fail();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void testSize()
	{
		assertEquals(0, stack.giveSizeStack());
		setUpEscenario2();
		assertEquals(2, stack.giveSizeStack());
		stack.pop();
		assertEquals(1, stack.giveSizeStack());
	}
	
}
