package t3_201720;

import junit.framework.TestCase;
import model.data_structures.Queue;

public class QueueTest extends TestCase
{
	Queue<Integer> queue = new Queue<Integer>();
	
	public void setUpEscenario1()
	{
		queue.enqueue(2);
	}
	
	public void SetUpEscenario2()
	{
		queue.enqueue(4);
		queue.enqueue(9);
	}
	
	public void testEnqueue()
	{
//		Queue vac�a.
		queue.enqueue(2);
		assertEquals(new Integer(2), queue.giveListQueue().giveLastNode().giveData());
		
//		Queue con un elemento.
		queue.enqueue(4);
		assertEquals(new Integer(4), queue.giveListQueue().giveLastNode().giveData());
	}
	
	public void testDequeue()
	{
//		Queue con un elemento. 
		setUpEscenario1();
		Integer a = queue.dequeue();
		assertEquals(new Integer(2), a);
		
//		Queue con dos elementos. 
		SetUpEscenario2();
		Integer b = queue.dequeue();
		assertEquals(new Integer(9), b);
		
		try 
		{
			queue.dequeue();
			fail();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
